module.exports = {
  extends: [
    "standard",
    "plugin:vue/recommended"
  ],
  rules: {
    "space-before-function-paren": ["error", "never"],
  }
};
