import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Router from './router'

Vue.use(VueRouter)

export default new Vue({
  el: '#app',
  ...App,
  router: Router
})
