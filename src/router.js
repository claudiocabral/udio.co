import VueRouter from 'vue-router'
import Home from '@/components/home'
import Music from '@/components/music'
import SoftwareDevelopment from './components/software_development'

export default new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', name: 'Home', component: Home },
    { path: '/music', name: 'Music', component: Music },
    { path: '/software', name: 'Software Development', component: SoftwareDevelopment }
  ]
})
