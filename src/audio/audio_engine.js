import MakeOscillator from '@/audio/oscillator'
import Midi from '@/audio/midi'

let audioCtx

export async function startAudioEngine(audioContext) {
  if (audioCtx !== undefined) audioCtx.close()
  audioCtx = audioContext
  const masterGain = audioCtx.createGain()
  masterGain.gain.value = 0.001
  masterGain.gain.exponentialRampToValueAtTime(0.1,
    audioCtx.currentTime + 4)
  masterGain.connect(audioCtx.destination)
  const carrier = MakeOscillator(audioCtx, {
    type: 'sine',
    frequency: 440,
    mul: 1,
    add: 0
  })
  const modulator = new OscillatorNode(audioCtx, {
    type: 'sine',
    frequency: 100
  })
  modulator.start()
  const modulatorGain = audioCtx.createGain()
  modulatorGain.gain.value = 600
  modulator.connect(modulatorGain)
  modulatorGain.connect(carrier.osc.frequency)
  const constant = new ConstantSourceNode(audioCtx, { offset: 300 })
  constant.start()
  const modulatorDetune = new OscillatorNode(audioCtx, {
    type: 'sine',
    frequency: 0.1
  })
  modulatorDetune.start()
  const modulatorDetuneGain = audioCtx.createGain()
  modulatorDetuneGain.gain.value = 50
  modulatorDetune.connect(modulatorDetuneGain)
  modulatorDetuneGain.connect(modulatorGain.gain)
  constant.connect(modulatorGain.gain)
  carrier.connect(masterGain)
  await Midi.startMidi()
  Midi.bind(0, (value) => {
    carrier.frequency(value)
  })
}

export function stopAudioEngine() {
  if (audioCtx !== undefined) audioCtx.close()
  audioCtx = undefined
}
