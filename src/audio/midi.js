export const MIDI_CC = 176
export const MIDI_NOTEON = 144
export const MIDI_NOTEOFF = 128
const CCArray = new Array(128)

for (let index = 0; index < CCArray.length; ++index) {
  CCArray[index] = function() {}
}

export default {
  startMidi() {
    if (!navigator.requestMIDIAccess) return
    navigator.requestMIDIAccess()
      .then(this.onMIDISuccess, err => console.log(err))
  },
  bind(cc, callback) {
    CCArray[cc] = callback
  },
  async onMIDISuccess(midi) {
    for (let input of midi.inputs) {
      // if (input[1].name !== 'nanoKONTROL2 MIDI 1') continue
      await input[1].open()
      input[1].onmidimessage = (message) => {
        if (message.data[0] !== MIDI_CC) return
        let cc = message.data[1]
        CCArray[cc](message.data[2])
      }
    }
  }
}
