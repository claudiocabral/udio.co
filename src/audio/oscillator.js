export default function MakeOscillator(context, options) {
  const oscillator = {
    osc: new OscillatorNode(context, {
      frequency: options.frequency ? options.frequency : 440,
      type: options.type ? options.type : 'sine',
      detune: options.detune ? options.detune : 0
    }),
    gain: new GainNode(context, {
      gain: options.mul ? options.mul : 1
    }),
    constant: new ConstantSourceNode(context, {
      offset: options.add ? options.add : 0
    }),
    connect(node) {
      this.gain.connect(node)
      this.constant.connect(node)
    },
    frequency(value) {
      this.osc.frequency.value = value
    },
    mul(value) {
      this.gain.gain.value = value
    },
    add(value) {
      this.constant.offset.value = value
    }
  }
  oscillator.osc.connect(oscillator.gain)
  oscillator.osc.start()
  oscillator.constant.start()
  return oscillator
}
